const Message = require('../models/message');
const Room = require('../models/room');

async function socketHandlers(io)
 {
  const currentTime = new Date().toLocaleString();
  console.log(`[${currentTime}] User connected`);
  console.log("hello1");

  io.on("connection",(socket)=>{
console.log("hello2");
io.emit('message',"welcome");
socket.on('joinRoom', async (data) => {
    
  try {
    console.log("hello3");
    const room = await Room.findById(data.roomId);
    if (!room) {
      throw new Error('Room not found');
    }
    room.users.push(data.userId);
    await room.save();
    socket.join(data.roomId);
    console.log(`[${currentTime}] User ${userId} joined room ${data.roomId}`);
  } catch (error) {
    console.error(error);
  }
});

socket.on('leaveRoom', async (roomId, userId) => {
  try {
    const room = await Room.findById(roomId);
    if (!room) {
      throw new Error('Room not found');
    }
    room.users = room.users.filter(id => id.toString() !== userId);
    await room.save();
    socket.leave(roomId);
    console.log(`[${currentTime}] User ${userId} left room ${roomId}`);
  } catch (error) {
    console.error(error);
  }
});

socket.on('sendMessage', async (messageData) => {
  try {
    const { senderId, receiverId, message } = messageData;
    
     
    const room = await Room.findOne({ users: { $all: [senderId, receiverId] } });
    if (!room) {
      throw new Error('Room not found');
    }

    const newMessage = new Message({ senderId, receiverId, message, roomId: room._id });
    await newMessage.save();
    io.to(room._id).emit('message', newMessage);
  } catch (error) {
    console.error(error);
  }
});

socket.on('disconnect', () => {
  console.log(`[${currentTime}] User disconnected`);
});
})
}

module.exports = socketHandlers;
