const Room = require('../models/room');
const User = require('../models/user');

async function createRoom(req, res) {
  try {
    const { userIds } = req.body;
    const users = await User.find({ _id: { $in: userIds } });
    if (users.length !== userIds.length) {
      return res.status(400).json({ message: 'One or more users do not exist' });
    }
    const existingRoom = await Room.findOne({ users: { $all: userIds } });
    if (existingRoom) {
      return res.status(400).json({ message: 'Room already exists for the provided users' });
    }
    const newRoom = new Room({ users: userIds });
    await newRoom.save();
    return res.status(201).json(newRoom);
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: 'Internal Server Error' });
  }
}

module.exports = {
  createRoom
};
