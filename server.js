const express = require('express');
const http = require('http');
 
const mongoose = require('mongoose');
const cors = require('cors');
const userRoutes = require('./routes/userRoutes');
const roomRoutes = require('./routes/roomRoutes');
const socketHandlers = require('./sockets/socketHandlers');

const app = express();
const server = http.createServer(app);
const io = require('socket.io')(server);

mongoose.connect('mongodb://localhost:27017/socketApp', {
  useNewUrlParser: true,
  useUnifiedTopology: true
});
const db = mongoose.connection;

app.use(cors());
app.use(express.json());
app.use('/users', userRoutes);
app.use('/rooms', roomRoutes);

 

server.listen(3050, () => {
  console.log('Server is running on port 3050');
  socketHandlers(io);
});
